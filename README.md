# HiFAST_FluxGain_Calibration
Author: Nekomata, NAOC, zmliu@nao.cas.cn

## 安装

需要安装HiFAST的最新版本，详见HiFAST安装（https://gitlab.com/jingyj/hifast）

需要额外安装astroquery包，


噪音管温度文件：下载 Tcal文件夹( https://pan.cstcloud.cn/s/AfnCB96cT2s 提取码：cqwy ) 放到家目录。如有新的噪音管文件，会一并更新到此目录。

## 计算流量增益系数

`HiFAST_FLUXGAIN`：输入定标源观测数据，得到流量转换系数（Flux Gain)。

- 示例

  ```
  fname="/data31/ZD2021_4/3C48_MultiBeamCalibration/20220729/3C48*M01_W*0001.fits"
  python HiFAST_FLUXGAIN_v1.py $fname -d 5 -m 5 -n 5 --nBs All --calname 3C48 --obsmode MultiBeamCalibration --outdir 
  ```

  程序首先会分离噪音管谱线对所有噪音管OFF谱线进行温度定标，考虑到噪音管ON在源上的稳定性较差因此源上的噪音管ON谱线会被舍弃。

  对扫描模式，会沿时间轴寻找Source ON位置与Source OFF位置，依靠高斯拟合得到 T_ON ，线性拟合得到T_OFF。
  对跟踪模式则会按照观测计划的观测时间（mjd）分离Source ON与Source OFF谱线，筛选掉对准误差较大的点之后ON-OFF得到T_source

  最后根据得到的T_source，和对应定标源的Flux Profile，计算得到每个频率点上的流量增益

- 其他参数

  - `-d -m -n`: 后面分别指定一个整数， 分别为delay时间、Cal on时间和Cal off时间除以谱线的采样时间。delay默认为0。
  - `--nBs` ：需要进行增益计算的波数，可以为1,All。默认为1
  - `--frange`：流量定标的频率范围，输出时会以该范围每5MHz取点计算增益，默认为[1300,1450]
  - `--noise_mode`：噪音管强度。 high 或者 low，默认为high。
  - `--noise_date`：选用哪天的噪音温度文件。例如设为 20190115 或 20200531. 如果设为auto则选取与谱线观测时间最近的噪音管文件来定标。(20200531的噪音管文件Beam19 XX 偏振 在约1060MHz处有个大的gap。)
  - `--obsmode`：定标源观测模式，可选项为Drift, MultiBeamCalibration, OnOff，需区分大小写。
  - `--t_src`：跟踪模式观测定标源时源上的时间，以秒为单位。默认为60。
  - `--outdir`：输出文件目录，默认为输入文件夹
  - `--calname`：定标源名，如果定标源存在于FluxProfiles.csv中则一般无需输入crd和flux
  - `--crd `：手动输入定标源坐标，以deg为单位，默认为None。
  - `--fluxProfilePara`：手动输入四位定标源流量轮廓参数，默认为None。
  - `--saveT`：是否保留温度中间文件*-T.hdf5，默认为True。
  - `--T_select`：跟踪模式定标时是否根据radec筛选定标采样点，默认为True。
  - `--rlim_src`：T_select步骤时有效坐标范围，以角秒为单位，默认为16。

- 输出文件名为 calname-obsdate_hm-obsmode-FluxGain-All.hdf5 （如果仅定标M01，则文件名中没有-All）。可以用h5py来读取。例如：

  ```
  import h5py
  f= h5py.File('**-FluxGain-All.hdf5','r')
  print(f.keys())
  f['mjd'][:]
  ```
- 输出的*T.hdf5文件中将保存观测温度、噪音管power、噪音管文件数值。
- 输出的*FluxGain.hdf5文件中保存K_Jy, count_Jy, ra, dec, ZD, 以及其他拟合参数。
  
  









